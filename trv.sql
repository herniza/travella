-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 05, 2018 at 09:47 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `travella`
--

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `albumId` int(10) NOT NULL,
  `userId` int(10) NOT NULL,
  `albumName` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `auditedTime` datetime NOT NULL,
  `auditedUserId` int(10) NOT NULL,
  `auditedActivity` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `chatId` int(10) NOT NULL,
  `userId1` int(10) NOT NULL,
  `userId2` int(10) NOT NULL,
  `auditedTime` datetime DEFAULT NULL,
  `auditedUserId` int(10) DEFAULT NULL,
  `auditedActivity` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `listpackage`
--

CREATE TABLE `listpackage` (
  `packageId` int(10) NOT NULL,
  `userId` int(10) NOT NULL,
  `packageName` varchar(50) NOT NULL,
  `packagePrice` int(20) NOT NULL,
  `packageDesc` text,
  `photoName` varchar(30) DEFAULT NULL,
  `auditedTime` datetime NOT NULL,
  `auditedUserId` int(10) NOT NULL,
  `auditedActivity` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `listpackage`
--

INSERT INTO `listpackage` (`packageId`, `userId`, `packageName`, `packagePrice`, `packageDesc`, `photoName`, `auditedTime`, `auditedUserId`, `auditedActivity`) VALUES
(1, 1, 'Yogyakarta Trip', 3000000, 'For 4 days, we offer you a chance to visit popular and beautiful places in Yoygyakarta. Experience a unique streetfood eating sensation in Yogyakarta at night.', 'yogya.jpg', '2018-06-06 00:00:00', 0, 'I'),
(3, 2, 'Lombok', 400000, 'Feel and create the unforgettable moments for 4 days in Lombok. Visit the places called heaven of the world and feel the sensation of diving in the beautiful sea.', 'lombok.jpg', '2018-06-15 00:00:00', 0, 'I'),
(4, 5, 'Lombok', 900000, 'Feel and create the unforgettable moments for 4 days in Lombok. Visit the places called heaven of the world and feel the sensation of diving in the beautiful sea.', 'lombok.jpg', '2018-06-15 00:00:00', 0, 'I'),
(5, 1, 'Lombok Trip', 3000000, 'For 4 days, we offer you a chance to visit popular and beautiful places in Yoygyakarta. Experience a unique streetfood eating sensation in Yogyakarta at night.', 'lombok.jpg', '2018-06-06 00:00:00', 0, 'I'),
(6, 1, 'Belitung Trip', 3500000, 'Feel and create the unforgettable moments for 4 days in Lombok. Visit the places called heaven of the world and feel the sensation of diving in the beautiful sea.', 'belitung.jpg', '2018-06-15 00:00:00', 0, 'I'),
(7, 2, 'WEDDING PHOTO', 3500000, 'asdffdfsfs', 'lombok.jpg', '2018-06-13 00:00:00', 0, 'I');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `messagesId` int(10) NOT NULL,
  `chatId` int(10) NOT NULL,
  `message` mediumtext NOT NULL,
  `auditedTime` datetime NOT NULL,
  `auditedUserId` int(10) NOT NULL,
  `auditedActivity` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `photoalbum`
--

CREATE TABLE `photoalbum` (
  `photoId` int(10) NOT NULL,
  `albumId` int(10) NOT NULL,
  `photoName` varchar(50) NOT NULL,
  `auditedTime` datetime NOT NULL,
  `auditedUserId` int(10) NOT NULL,
  `auditedActivity` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `reviewId` int(10) NOT NULL,
  `reviewerId` int(10) NOT NULL,
  `reviewingId` int(10) NOT NULL,
  `reviewDesc` text NOT NULL,
  `reviewDate` datetime DEFAULT NULL,
  `rating` int(1) NOT NULL,
  `auditedTime` datetime NOT NULL,
  `auditedUserId` int(10) NOT NULL,
  `auditedActivity` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`reviewId`, `reviewerId`, `reviewingId`, `reviewDesc`, `reviewDate`, `rating`, `auditedTime`, `auditedUserId`, `auditedActivity`) VALUES
(1, 2, 1, 'jasa nya bagus!', '2018-05-02 00:00:00', 3, '2018-05-02 00:00:00', 2, 'I'),
(2, 2, 1, 'tepat waktu. terima kasih', '2018-05-10 00:00:00', 2, '2018-06-30 08:10:24', 2, 'I'),
(3, 3, 1, 'humble and friendly. baik sekali!', '2018-05-17 04:21:15', 4, '2018-05-17 05:16:31', 4, 'I'),
(4, 3, 1, 'dddas', '2018-06-03 23:01:04', 2, '2018-06-03 23:01:04', 3, 'i');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userId` int(10) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` varchar(20) DEFAULT NULL,
  `description` text,
  `photoUser` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `whatsapp` varchar(20) DEFAULT NULL,
  `line` varchar(30) DEFAULT NULL,
  `website` varchar(50) DEFAULT NULL,
  `rating` int(1) NOT NULL DEFAULT '0',
  `companyAddress` varchar(100) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  `auditedTime` datetime DEFAULT NULL,
  `auditedUserId` int(10) DEFAULT NULL,
  `auditedActivity` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `username`, `email`, `password`, `role`, `description`, `photoUser`, `phone`, `whatsapp`, `line`, `website`, `rating`, `companyAddress`, `city`, `country`, `auditedTime`, `auditedUserId`, `auditedActivity`) VALUES
(1, 'Antonio Alfredo', 'photo@live.com', '5132ac40f025a99e7d9700446c3ab999', 'photographer', 'Dedicated and energetic photographer with 3+ years experience in photography.', 'me.jpg', '02121212', NULL, NULL, NULL, 5, NULL, 'Bali', 'Indonesia', '2018-05-16 00:00:00', 0, 'I'),
(2, 'Dara Travel', 'tourguide@live.com', '5132ac40f025a99e7d9700446c3ab999', 'tourguide', 'Dedicated and energetic. tour guide with 3+ years experience in travelling.', 'bal.png', '08147896523', NULL, NULL, NULL, 2, NULL, 'Lombok', 'Indonesia', '2018-05-16 00:00:00', 0, 'I'),
(3, 'user', 'user@live.com', '5132ac40f025a99e7d9700446c3ab999', 'user', '', 'logo.png', '02121212', NULL, NULL, NULL, 3, NULL, 'Palembang', 'Indonesia', '2018-05-16 00:00:00', 0, 'I'),
(4, 'Dumalang Snaps', 'jer@live.com', '5132ac40f025a99e7d9700446c3ab999', 'photographer', 'Dedicated and energetic photographer with 3+ years experience in photography.', 'jer.png', '02121212', NULL, NULL, NULL, 4, NULL, 'Yogyakarta', 'Indonesia', '2018-05-16 00:00:00', 0, 'I'),
(5, 'Tour Wins', 'win@live.com', '5132ac40f025a99e7d9700446c3ab999', 'tourguide', 'Dedicated and energetic photographer with 3+ years experience in photography.', 'win.png', '02121212', NULL, NULL, NULL, 1, NULL, 'Bali', 'Indonesia', '2018-05-16 00:00:00', 0, 'I');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`albumId`),
  ADD KEY `cekUserId` (`userId`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`chatId`),
  ADD KEY `cek_UserId1` (`userId1`),
  ADD KEY `cek_UserId2` (`userId2`);

--
-- Indexes for table `listpackage`
--
ALTER TABLE `listpackage`
  ADD PRIMARY KEY (`packageId`),
  ADD KEY `cek_userId` (`userId`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`messagesId`),
  ADD KEY `cek_chatId` (`chatId`);

--
-- Indexes for table `photoalbum`
--
ALTER TABLE `photoalbum`
  ADD PRIMARY KEY (`photoId`),
  ADD KEY `cek_albumId` (`albumId`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`reviewId`),
  ADD KEY `cek_reviewerId` (`reviewerId`),
  ADD KEY `cek_reviewingId` (`reviewingId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `albumId` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `chatId` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `listpackage`
--
ALTER TABLE `listpackage`
  MODIFY `packageId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `messagesId` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `photoalbum`
--
ALTER TABLE `photoalbum`
  MODIFY `photoId` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `reviewId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `album`
--
ALTER TABLE `album`
  ADD CONSTRAINT `cekUserId` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`);

--
-- Constraints for table `chat`
--
ALTER TABLE `chat`
  ADD CONSTRAINT `cek_UserId1` FOREIGN KEY (`userId1`) REFERENCES `users` (`userId`),
  ADD CONSTRAINT `cek_UserId2` FOREIGN KEY (`userId2`) REFERENCES `users` (`userId`);

--
-- Constraints for table `listpackage`
--
ALTER TABLE `listpackage`
  ADD CONSTRAINT `cek_userId` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`);

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `cek_chatId` FOREIGN KEY (`chatId`) REFERENCES `chat` (`chatId`);

--
-- Constraints for table `photoalbum`
--
ALTER TABLE `photoalbum`
  ADD CONSTRAINT `cek_albumId` FOREIGN KEY (`albumId`) REFERENCES `album` (`albumId`);

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `cek_reviewerId` FOREIGN KEY (`reviewerId`) REFERENCES `users` (`userId`),
  ADD CONSTRAINT `cek_reviewingId` FOREIGN KEY (`reviewingId`) REFERENCES `users` (`userId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
