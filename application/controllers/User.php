<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('user_model','User');
	}

	public function index($id = 0, $page='')
	{
		$title = "Info PT";
		$session = $this->session->userdata();
		$idPT = -1;
		
		if($session['role'] != null){

			if($session['role'] == 'photographer' || $session['role'] == 'tourguide')
			{
				$idPT = $session['userid'];
				$page='profilPT';
				$user = $this->User->getUserByID($idPT,$session['role']); 
			}else if($page == 'detailPhotographer'){
				
			}else if($page == 'profilUser'){
				
			}	


			$dataPageTab = array(
				'session' => $session,
				'user' => $user
			);	

			$dataTab = array(
				'session' => $session,
				'tab' => $this->load->view('info',$dataPageTab,TRUE),
				'page' => $page,
				'idPT' => $id,
				'user' => $user
			);	

			$mainContent = $this->load->view('templateTab',$dataTab,TRUE);
			//else load halaman detail photographer(?)
			$data = array
			(
				'mainContent' 	=> $mainContent,
				'title'			=> $title,
				'js'			=> 'home.js',
				'css'			=> 'review.css'
			);
			$this->load->view('template_all', $data);


		}
		
	}


}
