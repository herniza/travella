<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pricelist extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('pricelist_model','pricelist');
		$this->load->model('user_model','User');
		$session = $this->session->userdata();
		$this->load->helper(array('form', 'url'));
	}

	public function index()
	{
		$title = "Price List";
		$session = $this->session->userdata();
		$idPT = -1;
		$iudBtn = -1;

		$this->session->set_userdata('idDetail',$_GET['id']);

		if($session['role'] != null){
			$idDetail = $_GET['id'];
			if ($session['userid'] == $idDetail) {
				$iudBtn = 1;
				$page = 'profilPT';										
			}
			else $page = 'detailPhotographer';

			$user = $this->User->getUserByIDOnly($idDetail);
			$temp = new stdClass();
			$temp->userId 		= $user[0]['userId'];
			$temp->username 	= $user[0]['username'];
			$temp->email 		= $user[0]['email'];
			$temp->role 		= $user[0]['role'];
			$temp->description 	= $user[0]['description'];
			$temp->phone 		= $user[0]['phone'];
			$temp->whatsapp 	= $user[0]['whatsapp'];
			$temp->line 		= $user[0]['line'];
			$temp->website 		= $user[0]['website'];
			$temp->rating 		= $user[0]['rating'];
			$temp->companyAddress = $user[0]['companyAddress'];
			$temp->city 		= $user[0]['city'];
			$temp->country 		= $user[0]['country'];
			$temp->photoUser 	= $user[0]['photoUser'];


			$pricelist = $this->pricelist->getPackageByID($idDetail);

			//buat nampung hsil pricelist
			$dataPageTab = array(
				'session' => $session,
				'pricelist' => $pricelist
			);	


			$dataTab = array(
				'session' => $session,
				'tab' => $this->load->view('pricelist',$dataPageTab,TRUE),
				'page' => $page,
				'idPT' => $idDetail,
				'user' => $temp
			);	

			$mainContent = $this->load->view('templateTab',$dataTab,TRUE);
			
			$data = array
			(
				'mainContent' 	=> $mainContent,
				'idPT'			=> $idDetail,
				'title'			=> $title,
				'js'			=> 'home.js',
				'css'			=> 'pricelist.css'
			);
			$this->load->view('template_all', $data);


		}
		
	}

	public function do_upload(){
	    $config['upload_path'] = './assets/image/';
		$config['allowed_types']        = 'jpg|png';
		$config['max_size']             = 100;
		$config['max_width']            = 1024;
		$config['max_height']           = 768;

		$photoName = $_POST['photoName'];
 
		$this->load->library('upload', $config);
 
		if ( ! $this->upload->do_upload('photoName')){
			$error = array('error' => $this->upload->display_errors());
			echo json_encode('error');
		}else{
			$data = array('upload_data' => $this->upload->data());
			echo json_encode('sukses');
		}
	}

	public function do_upload2(){
		$status = "";
	    $msg = "";
	    $file_element_name = 'photoName';

	    if ($status != "error")
	    {
	        $config['upload_path'] = './assets/image/';
	        $config['allowed_types'] = 'jpg|png';
	        $config['max_size'] = 1024 * 8;
	        $config['encrypt_name'] = TRUE;
	 
	        $this->load->library('upload', $config);
	        echo explode('.', $_FILES[$file_element_name]['name']);
	        if (!$this->upload->do_upload($file_element_name))
	        {
	            $status = 'error';
	            $msg = $this->upload->display_errors('', '');
	        }
	        else
	        {

	            $data = $this->upload->data();
	            var_dump($data);
	            $param = array(
	            	'packageName' => $_POST['packageName'],
				    'packageDesc' => $_POST['packageDesc'],
				    'packagePrice' => $_POST['packagePrice'],
				    'userid'		=> $session['idDetail'],
	            );
	            $file_id = $this->pricelist_model->insertPriceList($data['file_name'], $_POST['title']);
	            if($file_id)
	            {
	                $status = "success";
	                $msg = "File successfully uploaded";
	            }
	            else
	            {
	                unlink($data['full_path']);
	                $status = "error";
	                $msg = "Something went wrong when saving the file, please try again.";
	            }
	        }
	        @unlink($_FILES[$file_element_name]);
	    }
	    echo json_encode(array('status' => $status, 'msg' => $msg));
	}
}
