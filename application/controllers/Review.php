<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Review extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('review_model','Review');
		$this->load->model('user_model','User');
	}

	public function index($page='',$id = 0)
	{
		$title = "Review";
		$session = $this->session->userdata();
		$idPT = -1;
		
		if($session['role'] != null){
			if($session['role'] == 'photographer' || $session['role'] == 'tourguide')
			{
				$idPT = $session['userid'];
				$page='profilPT';
				$user = $this->User->getUserByID($idPT,$session['role']);
				$reviews = $this->Review-> getReviewByID($idPT);
			}else if($page == 'detailPhotographer'){
				//as user
				$idPT = $id;
				$user = $this->User->getUserByID($idPT,'photographer');
				$reviews = $this->Review-> getReviewByID($idPT);
			}else if($page == 'profilUser'){
				//as user
				$user = $this->User->getUserByID($session['userid'],$session['role']);
				$reviews = $this->Review-> getReviewByUser($session['userid']);
			}	


			$dataPageTab = array(
				'session' => $session,
				'reviews' => $reviews
			);	

			$dataTab = array(
				'session' => $session,
				'tab' => $this->load->view('review',$dataPageTab,TRUE),
				'page' => $page,
				'idPT' => $id,
				'user' => $user
			);	

			$mainContent = $this->load->view('templateTab',$dataTab,TRUE);
			//else load halaman detail photographer(?)
			$data = array
			(
				'mainContent' 	=> $mainContent,
				'title'			=> $title,
				'js'			=> 'home.js',
				'css'			=> 'review.css'
			);
			$this->load->view('template_all', $data);


		}
		
	}

	public function addReview(){
		$review = $this->input->post('review');
		$idPhotograper = $this->input->post('idPhotograper');
		$rating = $this->input->post('rating');
		$session = $this->session->userdata();

		$this->Review->addReview($review, $idPhotograper,$rating,$session['userid']);
		//redirect('Review/index/'+$idPhotograper+'/detailPhotographer','refresh');
	}

}
