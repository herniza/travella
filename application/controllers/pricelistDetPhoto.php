<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pricelistDetPhoto extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('pricelist_model','pricelist');
		$this->load->model('user_model','User');
	}

	public function index()
	{
		$session = $this->session->userdata();
		$id = $session['idDetail'];

		$getPriceList = $this->pricelist->getPackageByID($id);
		$data ='';
		foreach ($getPriceList as $row) {
			$data .= '<div class="right1"><h5>';
			$data .= $row['packageName'];
			$data .= '</h5><h6>';
			$data .= $row['packagePrice'];
			$data .= '</h6><p>';
			$data .= $row['packageDesc'];
			$data .= '</p></div>';
		}
		echo $data;
	}

	public function image()
	{
		$session = $this->session->userdata();
		$id = $session['idDetail'];

		$getPriceList = $this->pricelist->getPackageByID($id);
		$data ='';
		foreach ($getPriceList as $row) {
			$data .= '<div class="left1"><img src="/travella/assets/image/';
			$data .= $row['photoName'];
			$data .= '" width="120%" height="120%"></div>';
		}

		echo $data;
	}
}
