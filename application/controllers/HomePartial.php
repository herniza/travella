<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class homePartial extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('home_model','home');
	}

	public function index()
	{
		$where = '';
		$flagLowerPrice = 0;
		$flagUpperPrice = 0;

		$rating = $this->input->post('rating');
		if($rating != null){
			$where .= ' u.rating in( ';
			foreach($rating as $row)
			{
				$where .= $row . ',';
			}
			$where .= ' "" ) ';
		}	

		$category = $this->input->post('category');
		if($category != null){
			if(strlen($where) > 1) $where .= 'and';
			$where .= ' u.role in( ';
			foreach($category as $row)
			{
				$where .= '"' . $row . '"' . ',';
			}
			$where .= ' "" ) ';
		}

		$location = $this->input->post('location');
		if($location != null){
			if(strlen($where) > 1) $where .= 'and';
			$where .= ' u.city in( ';
			foreach($location as $row)
			{
				$where .= '"' . $row . '"'. ',';
			}
			$where .= ' "" ) ';
		}

		if($this->input->post('lowerPrice') != null){
			if(strlen($where) > 1) $where .= 'and';
			$where .= ' packagePrice >= ' . $this->input->post('lowerPrice') . ' ';
			$flagLowerPrice = 1;
		}

		if($this->input->post('upperPrice') != null){
			if(strlen($where) > 1) $where .= 'and';
			$where .= ' packagePrice <= ' . $this->input->post('upperPrice'). ' ';
			$flagUpperPrice = 1;
		}
		
		$param = array(
			'where'		=> $where,
			'flagLower'		=> $flagLowerPrice,
			'flagUpper'		=> $flagUpperPrice,
		);

		$result = $this->home->getList($param);

		$data = '<div class="row">';

		foreach($result as $row)
		{
			$data .= '<div class="col">
			        	<div class="card"';
			$data .= ' id = "';
			$data .= $row['userId'];
			$data .='"><img src="/travella/assets/image/';

			$data .= $row['photoUser'];
			$data .= '" width="50px" height="170px" alt="Avatar" style="width:100%">
						  <div class="container"><p class="ptName">';
			$data .= $row['username'];
			$data .= '</p><p class="location">';
			$data .= $row['city'] . ', ' . $row['country'];
			$data .= '</p><p class="role">';

			if($row['role'] == 'photographer') $data .= 'Photographer';
			else $data .= 'Tour Guide';

			$data .= '</p></div></div></div>';

		}

		$data .= '</div>';
		echo $data;
	}
}
