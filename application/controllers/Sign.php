<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class sign extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->model('sign_model','sign');
		$this->load->helper('url');
		$this->load->library('session');
	}
	//untuk login
	public function index()
	{
		$this->load->view('signIn');
	}

	public function signOut()
	{
		$this->session->sess_destroy();
		redirect('sign');
	}

	public function signUp()
	{
		$this->load->view('signUp');
	}

	public function register(){
		$param = array(
			'email'			=> $this->input->post('email'),
			'password'		=> $this->input->post('password'),
			'phone'			=> $this->input->post('phone'),
			'role'			=> $this->input->post('role'),
		);

		$result = $this->sign->register($param);

		if($result == 0) $this->output->set_status_header('500');
	}

	public function validateSignin()
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$result = $this->sign->validateSignin($email, $password);

		if(count($result) != 1){
			$this->output->set_status_header('500');
		}
		else{
			$data = array(
				'userid' 	=> $result[0]['userid'],
				'username' 	=> $result[0]['username'],
				'email' 	=> $result[0]['email'],
				'role' 		=> $result[0]['role']
			);

			$this->session->set_userdata($data);
		}
	}
}
