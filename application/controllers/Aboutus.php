<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

 class aboutus extends CI_Controller
 {
 	
 	function __construct()
 	{
 		parent::__construct();
 		$this->load->model('aboutus_model','aboutus');
 		$this->load->helper('url');
 		$this->load->library('session');
 	}

 	//untk aboutus
 	public function index()
 	{
 		$title = "About Us";
		$session = $this->session->userdata();
		$mainContent = $this->load->view('aboutus',$session,TRUE);

		//else load halaman detail photographer(?)
		$data = array(
			'mainContent' 	=> $mainContent,
			'title'			=> $title,
			'js'			=> 'aboutus.js',
			'css'			=> 'aboutus.css'
		);
		$this->load->view('template_all', $data);
 	}
 } 


 