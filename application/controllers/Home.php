<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('home_model','home');
	}

	public function index()
	{
		$title = "Home";
		$session = $this->session->userdata();
		
		if($session['userid'] != null){
			if($session['role'] == 'user') $mainContent = $this->load->view('home',$session,TRUE);
			else{
				$url = 'pricelist?id='.$session['userid'];
				redirect('pricelist?id='.$session['userid']);
			}
			$data = array(
				'mainContent' 	=> $mainContent,
				'title'			=> $title,
				'css'			=> 'home.css'
			);
			$this->load->view('template_all', $data);
		}
	}
}
