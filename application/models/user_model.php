<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class user_model extends CI_Model{
	function getUserByID($id,$role){	
		$queryString = "SELECT * FROM users WHERE userId = ? AND role = ?";
		$query=$this->db->query($queryString, array($id,$role));
		$temp = new stdClass();
		$i = 0;

		$temp->userId = $query->row($i)->userId;
		$temp->username = $query->row($i)->username;
		$temp->email = $query->row($i)->email;
		$temp->role = $query->row($i)->role;
		$temp->description = $query->row($i)->description;
		$temp->phone = $query->row($i)->phone;
		$temp->whatsapp = $query->row($i)->whatsapp;
		$temp->line = $query->row($i)->line;
		$temp->website = $query->row($i)->website;
		$temp->rating = $query->row($i)->rating;
		$temp->companyAddress = $query->row($i)->companyAddress;
		$temp->city = $query->row($i)->city;
		$temp->country = $query->row($i)->country;
		$temp->photoUser = $query->row($i)->photoUser;

		return $temp;
	}	

	function getUserByIDOnly($id){
		$q = "SELECT * FROM users WHERE userId = '".$id."' and auditedactivity <> 'D'";
		$result = $this->db->query($q);

		return $result->result_array();
	}
}