<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class review_model extends CI_Model{
	function getReviewByID($id){	
		$result = array();
		$queryString = "SELECT reviewingID,reviewerID,username , photoUser , country , reviewDate, reviewDesc , b.rating FROM users a JOIN review b ON a.userId = b.reviewerID WHERE reviewingID = ?";
		$query=$this->db->query($queryString, array($id));
		for($i=0;$i<$query->num_rows();$i++)
		{
			$temp = new stdClass();

			$temp->reviewingID = $query->row($i)->reviewingID;
			$temp->reviewerID = $query->row($i)->reviewerID;
			$temp->username = $query->row($i)->username;
			$temp->country = $query->row($i)->country;
			$temp->reviewDate = $query->row($i)->reviewDate;
			$temp->reviewDesc = $query->row($i)->reviewDesc;
			$temp->rating = $query->row($i)->rating;
			$temp->photoUser = $query->row($i)->photoUser;

			array_push($result, $temp);
		}
		return $result;
	}	

	function getReviewByUser($id){	
		$result = array();
		$queryString = "SELECT reviewingID,reviewerID,username , photoUser , country , reviewDate, reviewDesc , b.rating FROM users a JOIN review b ON a.userId = b.reviewingID WHERE reviewerID = ?";
		$query=$this->db->query($queryString, array($id));
		for($i=0;$i<$query->num_rows();$i++)
		{
			$temp = new stdClass();

			$temp->reviewingID = $query->row($i)->reviewingID;
			$temp->reviewerID = $query->row($i)->reviewerID;
			$temp->username = $query->row($i)->username;
			$temp->country = $query->row($i)->country;
			$temp->reviewDate = $query->row($i)->reviewDate;
			$temp->reviewDesc = $query->row($i)->reviewDesc;
			$temp->rating = $query->row($i)->rating;
			$temp->photoUser = $query->row($i)->photoUser;

			array_push($result, $temp);
		}
		return $result;
	}	

	function addReview($review,$id,$rating,$userID){	
		$result = array();
		$queryString = "INSERT INTO review VALUE(null,?,?,?,NOW(),?,NOW(),?,'i')";
		$query=$this->db->query($queryString, array($userID,$id,$review,$rating,$userID));
	}	

}