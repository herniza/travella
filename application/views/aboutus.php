<body>
	<h1>ABOUT US</h1>
	<p>Travella is a market place that connects travelers to local people who are willing to be your "Travel Buddies". Where anyone can get and offer unique experiences, cultures, traditions, culinary, activities as well as learning new skills from local friends. Where our mission is to create happiness from the experience provided by the relationship between local friends and travelers. Not only that, we also provide photography services that are experts in the field to capture your precious moments while on the trip.</p>

	<footer class="row">
		<div class="col-md-4">
			<img src="<?php echo base_url();?>assets/image/twitter.png" width="70px" height="70px">
			<p>@travella</p>
		</div>
		<div class="col-md-4">
			<img src="<?php echo base_url();?>assets/image/ig.png" width="70px" height="70px">
			<p>@travella</p>	
		</div>
		<div class="col-md-4">
			<img src="<?php echo base_url();?>assets/image/fb.png" width="70px" height="70px">
			<p>Travella Indonesia</p>
		</div>
	</footer>

	
</body>