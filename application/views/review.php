
<script type="text/javascript">
	function addReview(){
		var id = document.getElementById("idPhotograper").value
		var datas = {
			review: document.getElementById("txtReview").value,
			rating: document.getElementById("txtRating").value,
			idPhotograper: id
		};
		$.ajax({
	       type: "POST",
	       url: '<?php echo site_url('Review/addReview'); ?>',
	       data: datas,
	       success: function(data){
	       		alert("Success");
	       		document.getElementById("txtReview").value="";
	       		document.getElementById("txtRating").value="0";
	       		
	       	}
		});
	}

</script>

<?php if($session['role'] == 'user'){ ?>
<div>
	<div class="offset-10 col-md-2">
  		<button type="button" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Give Review!</button>
	</div>
</div>
<?php } ?>


<!-- part 1 -->
<?php 
	$flag=false;
	for($i=0;$i<count($reviews);$i++){
		$flag=true;
?>
<div class="row jarak" >
	<div class="col-md-3" style="margin-top: 20px">		
		<img src="<?php echo base_url();?>assets/image/<?php echo $reviews[$i]->photoUser; ?>" class="col-md-4">
		<div style="display: inline-block;">
			<h1 class="font-user"><?php echo $reviews[$i]->username; ?>, <?php echo $reviews[$i]->country; ?></h1>
			<p><?php echo $reviews[$i]->reviewDate; ?></p>
		</div>			
	</div>
	<div class="col-md-6 box-tengah font-review">
		<p><?php echo $reviews[$i]->reviewDesc; ?></p>
	</div>
	<div class="col-md-3 center">
		<div style="margin-top: 15%;">
			<?php 
			$star = 5;
			for($j=0;$j<$reviews[$i]->rating;$j++){ ?>
				<img src="<?php echo base_url();?>assets/image/s.png" class="star">
				
			<?php $star--;
			} 

			for($j=0;$j<$star;$j++){
			?>
				<img src="<?php echo base_url();?>assets/image/Star_rating_1_of_5.png" class="star">
			<?php } ?>
		</div>
	</div>
</div>

<div class="devider"></div>

<?php }
if(!$flag){
 ?>
 NO REVIEW

<?php } ?>


<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title">Add Review</h5>
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
      	<div id="form">	
      		<input type="hidden" id="idPhotograper" value="<?php echo $reviews[0]->reviewingID; ?>">
			<table>
				<tr>
					<td>Rating</td>
				</tr>
				<tr>
					<td>
						<input type="number" min="0" id="txtRating" value="0">
					</td>
				</tr>
				<tr>
					<td>Review</td>
				</tr>
				<tr>
					<td>
						<textarea id="txtReview"></textarea>
					</td>
				</tr>
			</table>
			<input type="submit" name="submit" onclick="addReview()" value="Submit">			
		</div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
  
</div>
</div>
