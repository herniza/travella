<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery-ui.css"></link>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery-ui.theme.css"></link>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery-ui.structure.css"></link>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.css"></link>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.css"></link>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font.css"></link>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/sign.css"></link>

	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/ajaxfileupload.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.bundle.js"></script>
	<title>Sign In</title>

	<script type="text/javascript">
		function submitForm(){
			var datas = {
				email: document.getElementById("email").value,
				password: document.getElementById("password").value
			};
			$.ajax({
		       type: "POST",
		       url: '<?php echo site_url('sign/validateSignin'); ?>',
		       data: datas,
		       success: function(data){
		       		window.location = "<?php echo site_url('home');?>";
		       	},
		       error: function(res, status) {
					$("#errorMsg").show();
		       }
			});
		}

		$(document).ready(function() {
			$("#errorMsg").hide();
		});
	</script>
</head>

<body>
	<div id="bigDiv">
		<div id="leftSide">
			<img id="imgLogo" src="<?php echo base_url();?>assets/image/logo.png" width="300px" height="300px">
			<br>
			<p id="titleLeftSide">HELLO<br>WORLD!</p>
			<p class="leftSideWord" style="font-size: 20px;">welcome to travella, <br>your traveling buddy</p>
			<br><br>
			<p class="leftSideWord">Don't have an account?<br><a href="<?php echo site_url('sign/signUp') ?>">Sign Up Here</a><br>and discover our benefits.</p>
			<img class="sosmed" style="margin: 15% 5% 0% 15%;" src="<?php echo base_url();?>assets/image/fb.png" width="40px" height="40px">
			<img class="sosmed" style="margin: 15% 5% 0% 5%;" src="<?php echo base_url();?>assets/image/ig.png" width="40px" height="40px">
			<img class="sosmed" style="margin: 15% 5% 0% 5%;" src="<?php echo base_url();?>assets/image/twitter.png" width="40px" height="40px">
		</div>

		<div id="rightSide">
			<div id="signIn">
				<p id="welcomeRightSide">Welcome,</p>

				<div id="form">	
					<p style="margin: -5% 0% 5% 1%; color: red;font-size: 11px;" id="errorMsg">your email or password is incorrect.</p>

					<table>
						<tr>
							<td>Email</td>
						</tr>
						<tr></tr>
							<td><input type="text" id="email" name="email"></td>
						</tr>
						<tr>
							<td>Password</td>
						</tr>
						<tr>
							<td><input type="password" id="password" name="password"></td>
						</tr>
					</table>

					<input type="submit" name="submit" onclick="submitForm()" value="Sign In">
				</div>
			</div>
		</div>
	</div>
</body>
</html>