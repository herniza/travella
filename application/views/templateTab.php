<div class="big-padding">
	<div class="row">
		<div class="col-md-2">
			<img src="<?php echo base_url();?>assets/image/<?php echo $user->photoUser; ?>" class="img100">
		</div>
		<div class="col-md-10">
			<div class="row">
				<h1><?php echo $user->username; ?></h1>
			</div>
			<div class="row">
				<?php 
				$star = 5;
				for($j=0;$j<$user->rating;$j++){ ?>
					<img src="<?php echo base_url();?>assets/image/s.png" class="star" style="height: 3em;">
					
				<?php $star--;
				} 

				for($j=0;$j<$star;$j++){
				?>
					<img src="<?php echo base_url();?>assets/image/Star_rating_1_of_5.png" class="star" style="height: 3em;">
				<?php } ?>
			</div>
		</div>
	</div>
	
	<div class="row">		
		<div class="col-md-12" style="margin:3em 0;">	
  			<?php if($page == 'profilPT'){ ?>				

				<a  href="<?php echo base_url();?>User" class="menu-tab">Info</a>
				<a href="#" class="menu-tab">Album</a>				
				<a href="<?php echo base_url();?>Review" class="menu-tab">Review</a>
	  			<a href="<?php echo site_url('pricelist?id='); echo $idPT;?>" class="menu-tab">Price List</a>	
				<a href="#" class="menu-tab">Personal Information</a>	

			<?php }else if($page == 'detailPhotographer'){ ?>

				<a  href="#" class="menu-tab">Info</a>
				<a href="#" class="menu-tab">Album</a>				
				<a href="<?php echo site_url('Review/index/'); echo $page; echo '/'; echo $idPT;?>" class="menu-tab">Review</a>
	  			<a href="<?php echo site_url('pricelist?id='); echo $idPT;?>" class="menu-tab" >Price List</a>	

			<?php }else if($page == 'profilUser'){ ?>

				<a href="#" class="menu-tab">Personal Information</a>	
				<a href="<?php echo base_url().'Review/index/'.$page;?>" class="menu-tab">Review</a>
				<a href="<?php echo site_url('pricelist?id='); echo $idPT; ?>" class="menu-tab">Message</a>
			<?php } ?>
		</div>
		<div class="col-md-12">
		 	<?php if($tab!=null)
			echo $tab;?>
		</div>
	  	</div>
	</div>
</div>