<body>
	<div class="cont">
		<div class="left">
			<div class="left1">
				<img src="<?php echo base_url();?>assets/image/pricelist/1/yogya.jpg" width="120%" height="120%">
			</div>
			<div class="left2">
				<img src="<?php echo base_url();?>assets/image/pricelist/1/lombok.jpg" width="120%" height="120%">
			</div>
			<div class="left3">
				<img src="<?php echo base_url();?>assets/image/pricelist/1/belitung.jpg" width="120%" height="120%">
			</div>
		</div>

		<div class="right">
			<div class="right1">
				<h5>Yogyakarta Trip</h5>
				<h6>IDR 3.000.000</h6>
				<p>For 4 days, we offer you a chance to visit popular and beautiful places in Yoygyakarta. Experience a unique streetfood eating sensation in Yogyakarta at night.</p>

			</div>
			<div class="right2">
				<h5>Belitung Trip</h5>
				<h6>IDR 1.625.000</h6>
				<p>For 3 days, Be the part of beautifulness in Belitung, create beautiful memories with local people and feel the sensation of exploring belitung island </p>

			</div>
			<div class="right3">
				<h5>Lombok Trip</h5>
				<h6>IDR 3.500.000</h6>
				<p>Feel and create the unforgettable moments for 4 days in Lombok. Visit the places called heaven of the world and feel the sensation of diving in the beautiful sea.</p>

				<div id="bottom3">
				</div>
			</div>
		</div>
			
	</div>
</body>

