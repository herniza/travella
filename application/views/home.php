<script type="text/javascript">
	function submitFilter()
	{
		var category = $('.category:checked').map(function(_, el) {
		    return $(el).val();
		}).get();

		var rating = $('.ratingStar:checked').map(function(_, el) {
		    return $(el).val();
		}).get();

		var location = $('.location:checked').map(function(_, el) {
		    return $(el).val();
		}).get();

		var datas = {
			lowerPrice: document.getElementById("lowerPrice").value,
			upperPrice: document.getElementById("upperPrice").value,
			category: category,
			rating: rating,
			location: location
		};
		$.ajax({
	       type: "POST",
	       url: '<?php echo site_url('HomePartial'); ?>',
	       data: datas,
	       success: function(data){
	       		$( ".table" ).html(data);
	       	}
		});
		
	}

	$(document).ready(function() {
		$(".table").load("<?php echo site_url('HomePartial');?>");

		$(document).on('click', "div.card", function() {
		     window.location = "<?php echo site_url('pricelist?id=');?>" + $(this).attr("id");   
		});
	});
</script>
<body>
	<div id="leftDiv">
		<div id="filterSubmit">
		<p class="filterP">Category</p>
		<input type="checkbox" class="category" value="photographer"> &nbsp; Photographer
		<br>
		<input type="checkbox" class="category" value="tourGuide"> &nbsp; Tour Guide
		<br>

		<p class="filterP">Price Range</p>	
		<div id="priceRange">
			<div id="lowerPriceDiv">
				<label>Rp.</label>

				<input type="text" width="10%" id="lowerPrice"><br>
			</div>

			<div id="upperPriceDiv">
				<label>Rp.</label>
				<input type="text" id="upperPrice"><br>
			</div>
		</div>

		<p class="filterP">Rating</p>
		<input type="checkbox" class="ratingStar" value="1"> &nbsp; 
			<span class="fa fa-star checked"></span>
		<br>
		<input type="checkbox" class="ratingStar" value="2"> &nbsp; 
			<span class="fa fa-star checked"></span>
			<span class="fa fa-star checked"></span>
		<br>
		<input type="checkbox" class="ratingStar" value="3"> &nbsp; 
			<span class="fa fa-star checked"></span>
			<span class="fa fa-star checked"></span>
			<span class="fa fa-star checked"></span>
		<br>
		<input type="checkbox" class="ratingStar" value="4"> &nbsp; 
			<span class="fa fa-star checked"></span>
			<span class="fa fa-star checked"></span>
			<span class="fa fa-star checked"></span>
			<span class="fa fa-star checked"></span>
		<br>
		<input type="checkbox" class="ratingStar" value="5"> &nbsp; 
			<span class="fa fa-star checked"></span>
			<span class="fa fa-star checked"></span>
			<span class="fa fa-star checked"></span>
			<span class="fa fa-star checked"></span>
			<span class="fa fa-star checked"></span>
		<br>

		<p class="filterP">Location</p>
		<input type="checkbox" class="location" value="lombok"> &nbsp; Lombok
		<br>
		<input type="checkbox" class="location" value="yogyakarta"> &nbsp; Yogyakarta
		<br>
		<input type="checkbox" class="location" value="bali"> &nbsp; Bali
		<br><br>

		<input type="submit" name="submit" id="btnSubmitFilter" onclick="submitFilter()">
	</div>
	</div>

	<div id="rightDiv">
		<div id="listPT">
			<div class="table">
			   
			</div>
		</div>
	</div>
</body>