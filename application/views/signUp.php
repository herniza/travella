<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery-ui.css"></link>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery-ui.theme.css"></link>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery-ui.structure.css"></link>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.css"></link>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font.css"></link>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/sign.css"></link>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/signUp.css"></link>

	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/ajaxfileupload.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.bundle.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/popper.js"></script>
	<title>Sign Up</title>

	<script type="text/javascript">
		function validateRegist(datas){
			var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
			 
			$("#emailErrorMsg").hide();
			$("#phoneErrorMsg").hide();
			$("#roleErrorMsg").hide();
           
            if (reg.test(datas['email']) == false || datas['email'] == null) 
            {
            	$("#emailErrorMsg").show();
              	document.getElementById("emailErrorMsg").innerHTML = "Enter a valid email address.";
              	return false;
            }
            else if (isNaN(datas['phone']) || datas['phone'] == "") 
            {
            	$("#phoneErrorMsg").show();
              	document.getElementById("phoneErrorMsg").innerHTML = "Phone only contains number.";
              	return false;
            }
            else if (datas['role'] == 0) 
            {
            	$("#roleErrorMsg").show();
              	document.getElementById("roleErrorMsg").innerHTML = "Please select role.";
              	return false;
            }
            else{
            	return true;
            }
		}

		function submitForm(){
			var datas = {
				email: document.getElementById("email").value,
				password: document.getElementById("password").value,
				phone: document.getElementById("phone").value,
			 	role: document.getElementById("role").value
			};

			if(validateRegist(datas)){
				$.ajax({
			       type: "POST",
			       url: '<?php echo site_url('sign/register'); ?>',
			       data: datas,
			       success: function(data){
			       		window.location = "<?php echo site_url('sign');?>";
			       	},
			       error: function(res, status) {
						$("#errorMsg").show();
			       }
				});
			}
		}

		$(document).ready(function() {
			$("#emailErrorMsg").hide();
			$("#phoneErrorMsg").hide();
			$("#roleErrorMsg").hide();
			$("#errorMsg").hide();
		});
	</script>
</head>

<body>
	<div id="bigDiv">
		<div id="leftSide">
			<img id="imgLogo" src="<?php echo base_url();?>assets/image/logo.png" width="300px" height="300px">
			<br>
			<p id="titleLeftSide">HELLO<br>WORLD!</p>
			<p class="leftSideWord" style="font-size: 20px;">welcome to travella, <br>your traveling buddy</p>
			<br><br>
			<p class="leftSideWord">Already a member?<br><a href="<?php echo site_url('sign') ?>">Sign In Here</a><br>and discover our benefits.</p>
			<img class="sosmed" style="margin: 15% 5% 0% 15%;" src="<?php echo base_url();?>assets/image/fb.png" width="40px" height="40px">
			<img class="sosmed" style="margin: 15% 5% 0% 5%;" src="<?php echo base_url();?>assets/image/ig.png" width="40px" height="40px">
			<img class="sosmed" style="margin: 15% 5% 0% 5%;" src="<?php echo base_url();?>assets/image/twitter.png" width="40px" height="40px">
		</div>

		<div id="rightSide">
			<div id="signUp">
				<p id="welcomeRightSide">Welcome,</p>

				<p style="margin: 0% 0% 5% 13%; color: red;font-size: 11px;" id="errorMsg"> Email has already registered. </p>
				<div id="form">	
					<table>
						<tr>
							<td>Email</td>
						</tr>
						<tr>
							<td><p style="margin: 0% 0% 1% 0%; color: red;font-size: 11px;" id="emailErrorMsg"></p></td>
						</tr>
						<tr>
							<td><input type="text" id="email" name="email"></td>
						</tr>
						<tr>
							<td>Password</td>
						</tr>
						<tr>
							<td><input type="password" id="password" name="password"></td>
						</tr>
						<tr>
							<td>Phone Number</td>
						</tr>
						<tr>
							<td><p style="margin: 0% 0% 1% 0%; color: red;font-size: 11px;" id="phoneErrorMsg"></p></td>
						</tr>
						<tr>
							<td><input type="text" id="phone" name="phone"></td>
						</tr>
						<tr>
							<td>Role</td>
						</tr>
						<tr>
							<td><p style="margin: 0% 0% 1% 0%; color: red;font-size: 11px;" id="roleErrorMsg"></p></td>
						</tr>
						<tr>
							<td>
								<select id="role" name="role">
									<option value="0">Please Select</option>
									<option value="photographer">Photographer</option>
									<option value="tourguide">Tour Guide</option>
									<option value="user">User</option>
								</select>
							</td>
						</tr>
					</table>

					<input type="submit" name="submit" onclick="submitForm()" value="Sign Up">
				</div>
			</div>
		</div>
	</div>
</body>
</html>