<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery-ui.css"></link>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery-ui.theme.css"></link>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery-ui.structure.css"></link>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.css"></link>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.css"></link>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font.css"></link>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css"></link>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/<?php echo $css;?>"></link>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/fontawesome.css">
	
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/ajaxfileupload.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.bundle.js"></script>

	<title><?php echo $title;?></title>
</head>

<body>
	<div id="header">
		<img src="<?php echo base_url();?>assets/image/logo.png" width="85px" height="85px">
		<ul class="medium">
			<li><a href="<?php echo site_url('home') ?>">HOME</a></li>
			<li><a href="<?php echo site_url('aboutus') ?>">ABOUT US</a></li>
			<li>MY PROFILE</li>
			<li class="regular" style="border: 1px solid black; border-radius: 20px;"><a href="<?php echo site_url('sign/signOut') ?>">sign out</a></li>
		</ul>
	</div>


	<div id="bigContent">
		<?php if($mainContent!=null)
				echo $mainContent;?>
	</div>

	<div id="footer">
		<div class="column leftFooter">
			<img src="<?php echo base_url();?>assets/image/logo.png" width="110px" height="110px">
			<p>@ 2018. All Rights Reserved. </p>
		</div>
		<div class="column middleFooter" id="contact">
			<h6>CONTACT</h6>
			<br>
			<p>(021) 888-999</p>
			<p style="margin-bottom: 10px;">info@travella.com</p>
			<div style="display: inline;">
				<img src="<?php echo base_url();?>assets/image/fb.png" width="20px" height="20px">
				<img src="<?php echo base_url();?>assets/image/twitter.png" width="20px" height="20px">
				<img src="<?php echo base_url();?>assets/image/ig.png" width="20px" height="20px">
			</div>
		</div>
		<div class="column rightFooter" id="address">
			<h6>ADDRESS</h6>
			<br>
			<p>Jalan Setiabudi No. 15A</p>
			<p>Jakarta Pusat</p>
			<p>Indonesia</p>
		</div>
	</div>
</body>
</html>